### Jak to funguje
Spustí lighthouse report nad url několikrát po sobě. Pro každý test vygeneruje report do složky reports s názvem souboru ve formátu "{UNIX_TIME_STAMP}.lhreport.html". Na konci zobrazí průměrné celkové skůre ze všech testů.

### Instalace dependencí

```bash
npm install
```

### Spuštění

```bash
node index.js
```

### Parametry

| Pořadí | Význam         | Default               |
| ------ | -------------- | --------------------- |
| 1      | URL stránky    | https://www.aukro.cz/ |
| 2      | Počet spuštění | 10                    |

### Plný příklad
```bash
node index.js https://lb-dev-web.ext.agdc.cloud/ 3
```
