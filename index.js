const fs = require('fs');
const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');

const args = process.argv.slice(2);
const url = args.length ? args[0] : 'https://www.aukro.cz/';
const runs = args.length > 1 ? Number(args[1]) : 10;

const results = [];

(async () => {
  for (let i = 0; i < runs; i++) {
    results.push(await runLighthouse())
  }

  console.log(`all runs finished. Average score was: ${results.reduce((r, acc) => r + acc) / results.length * 100}`)
})();

async function runLighthouse() {
  const chrome = await chromeLauncher.launch({chromeFlags: ['--headless']});
  const options = {logLevel: 'info', output: 'html', onlyCategories: ['performance'], port: chrome.port};
  const runnerResult = await lighthouse(url, options);

  const reportHtml = runnerResult.report;
  fs.writeFileSync(`./reports/${generateReportFileName()}`, reportHtml);

  console.log('Report is done for', runnerResult.lhr.finalUrl);
  console.log('Performance score was', runnerResult.lhr.categories.performance.score * 100);

  await chrome.kill();

  return runnerResult.lhr.categories.performance.score;
}

function generateReportFileName() {
  return `${(new Date()).getTime()}.lhreport.html`;
}
